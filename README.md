<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p>Specifications:</p>

- Laravel 8
- PhpUnit sqlite
- Auth
- Vue
- Axios

<p>Ejecute this commands to run the proyect:</p>

```
composer install
cp .env.example .env
php artisan key:generate
npm install 
npm run dev
```